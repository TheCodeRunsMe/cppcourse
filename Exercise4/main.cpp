#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>

#include <QBoxPlotSeries>
#include <QBoxSet>
#include <QFile>
#include <QString>
#include <QTextStream>
#include <QVBoxLayout>
#include <QVector>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

QT_USE_NAMESPACE
using namespace std;

// a.
class DailyMeteoEvent {
  string theCityName;
  int theDay, theMonth, theYear, theJulianDay;
  double theMaxT, theMinT;
public:
  DailyMeteoEvent(int day, int month, int year, int julianDay, double maxT,
                  double minT)
      : theDay(day), theMonth(month), theYear(year), theJulianDay(julianDay),
        theMaxT(maxT), theMinT(minT) {
    this->SetCityName("Seville");
  }
  string GetCityName() const { return theCityName; }
  int GetDay() const { return theDay; }
  int GetMonth() const { return theMonth; }
  int GetYear() const { return theYear; }
  int GetJulianDay() const { return theJulianDay; }
  double GetMaxT() const { return theMaxT; }
  double GetMinT() const { return theMinT; }
  void SetCityName(const string &name) { theCityName = name; }
  void SetDay(int day) { theDay = day; }
  void SetMonth(int month) { theMonth = month; }
  void SetYear(int year) { theYear = year; }
  void SetJulianDay(int JulianDay) { theJulianDay = JulianDay; }
  void SetMaxT(int T) { theMaxT = T; }
  void SetMinT(int T) { theMinT = T; }
  double GetMeanOfDate(int julianday);
  // required to use indexOf method of QList
  bool operator==(const DailyMeteoEvent &a) const {
    return this->GetJulianDay() == a.GetJulianDay();
  }
};

QVector<DailyMeteoEvent> days;
//b
double DailyMeteoEvent::GetMeanOfDate(int julianday) {
    // nullptr C++11 std literal used for uninitialised pointers.
    const DailyMeteoEvent *dayOfInterest = nullptr;
    for (const auto &d : days)
        if (d.GetJulianDay() == julianday) dayOfInterest = &d;
    if (!dayOfInterest)
        throw invalid_argument("Julian day is not in the data");
    if (dayOfInterest->GetMaxT() == -274 || dayOfInterest->GetMinT() == -274)
        throw logic_error("Min or Max Temperature is absent");
    return (dayOfInterest->GetMaxT() + dayOfInterest->GetMinT()) / 2;
}

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  int year1;
  cout << "enter an arbitrary year to plot mothly means and extrema of next 20 "
          "years: ";
  cin >> year1;
  if (year1 < 1960 || year1 > 1999)
    throw domain_error("years are not within available data");
  int year2 = year1 + 20;
  QFile dataFile("data.csv");
  if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text))
    return 2;
  // c.
  QTextStream in(&dataFile);
  QString line = in.readLine();
  while (!line.isNull()) {
    line = in.readLine();
    // skip double lines between years
    if (!line.isEmpty()) {
      QStringList fields = line.split(";");
      if (fields[4].isEmpty()) fields[4] = "-274"; // impossible Temperature
      if (fields[5].isEmpty()) fields[5] = "-274"; // for unavailable datum
      DailyMeteoEvent Day(fields[0].toInt(), fields[1].toInt(),
                          fields[2].toInt(), fields[3].toInt(),
                          fields[4].toDouble(), fields[5].toDouble());
      days.push_back(Day);
    }
  }
  // c.i
  QVector<int> year = {1960};
  QVector<double> minYear, maxYear; // refilled for each year
  QVector<double> maximum, minimum;

  QLineSeries *amioy = new QLineSeries();
  QLineSeries *amaoy = new QLineSeries();

  for (auto &day : days) {
    if (day.GetYear() == year.back()) {
      // disregarding invalid data
      if (day.GetMinT() != -274) minYear.push_back(day.GetMinT());
      if (day.GetMaxT() != -274) maxYear.push_back(day.GetMaxT());
    } else {
      // ranges::* is c++20 std finding algorith with constraints on
      // concepts so that if a non-sortable object is tried we will get
      // warning from analyzer or error from compiler
      double Tma = *ranges::max_element(maxYear);
      maximum.push_back(Tma);
      amaoy->append(year.back(), Tma);
      double Tmi = *ranges::min_element(minYear);
      minimum.push_back(Tmi);
      amioy->append(year.back(), Tmi);
      minYear.clear();
      maxYear.clear();
      minYear.push_back(day.GetMinT());
      maxYear.push_back(day.GetMaxT());
      year.push_back(day.GetYear());
    }
  }
  double Tma = *ranges::max_element(maxYear);
  maximum.push_back(Tma);
  amaoy->append(year.back(), Tma);
  double Tmi = *ranges::min_element(minYear);
  minimum.push_back(Tmi);
  amioy->append(year.back(), Tmi);

  cout << "Mean of julian day " << 2459215 << ": "
       << days[0].GetMeanOfDate(2459215) << endl;

  QChart *chart0 = new QChart();
  chart0->legend()->hide();
  chart0->addSeries(amioy);
  chart0->addSeries(amaoy);
  chart0->createDefaultAxes();
  chart0->axes(Qt::Horizontal).first()->setTitleText("Year");
  chart0->axes(Qt::Vertical).first()->setTitleText(
      QString::fromUtf8("Temperature (C\u00B0)"));
  chart0->setTitle("c.i: Absolute minimum/maximum of the years");

  QChartView *chartView0 = new QChartView(chart0);
  chartView0->setRenderHint(QPainter::Antialiasing);
  chartView0->chart()->setTheme(QChart::ChartThemeBlueCerulean);

  cout << "Absolute minimum temperature of all time is: "
       << *ranges::min_element(minimum)
       << QString::fromUtf8(" C\u00B0").toStdString() << endl;
  cout << "Absolute maximum temperature of all time is: "
       << *ranges::max_element(maximum)
       << QString::fromUtf8(" C\u00B0").toStdString() << endl;

  // c.ii, d.ii, d.iv
  QVector<QVector<double>> maxMonth(12), minMonth(12), mim20(12), mam20(12);
  QBoxPlotSeries *ofMonth = new QBoxPlotSeries();
  QBoxPlotSeries *of20y = new QBoxPlotSeries();
  QBoxPlotSeries *meanStdoMa = new QBoxPlotSeries();
  QBoxPlotSeries *meanStdoMi = new QBoxPlotSeries();
  for (auto &day : days) {
    if (day.GetMaxT() != -274) {
      maxMonth[day.GetMonth() - 1].push_back(day.GetMaxT());
      // for 20 years
      if (day.GetYear() >= year1 && day.GetYear() < year2)
        mam20[day.GetMonth() - 1].push_back(day.GetMaxT());
    }
    if (day.GetMinT() != -274) {
      minMonth[day.GetMonth() - 1].push_back(day.GetMinT());
      // for 20 years
      if (day.GetYear() >= year1 && day.GetYear() < year2)
        mim20[day.GetMonth() - 1].push_back(day.GetMinT());
    }
  }
  QVector<double> maxInMonth, minInMonth, amam, amim, stdmam, stdmim;
  for (auto &m : maxMonth) {
    maxInMonth.push_back(*ranges::max_element(m));
    auto meanmam = accumulate(m.begin(), m.end(), 0); // for average of max
    amam.push_back(meanmam / m.count());
    double std = 0;
    for (auto d : m) std += pow(meanmam / m.count() - d, 2);
    stdmam.push_back(sqrt(std / (m.count() - 1)));
  }
  for (auto &m : minMonth) {
    minInMonth.push_back(*ranges::min_element(m));
    auto meanmim = accumulate(m.begin(), m.end(), 0);
    amim.push_back(meanmim / m.count());
    double std = 0;
    for (auto d : m) std += pow(meanmim / m.count() - d, 2);
    stdmim.push_back(sqrt(std / (m.count() - 1)));
  }
  QVector<double> max20, min20, amam20, amim20;
  for (auto &m : mam20) {
    max20.push_back(*ranges::max_element(m));
    amam20.push_back(accumulate(m.begin(), m.end(), 0) / m.count());
  }
  for (auto &m : mim20) {
    min20.push_back(*ranges::min_element(m));
    amim20.push_back(accumulate(m.begin(), m.end(), 0) / m.count());
  }
  for (int i = 0; i < 12; i++) {
    QBoxSet *box = new QBoxSet(QString::number(i + 1));
    box->setValue(QBoxSet::LowerExtreme, minInMonth[i]);
    box->setValue(QBoxSet::UpperExtreme, maxInMonth[i]);
    box->setValue(QBoxSet::Median, amim[i]);
    box->setValue(QBoxSet::LowerQuartile, amim[i]);
    box->setValue(QBoxSet::UpperQuartile, amam[i]);
    ofMonth->append(box);
  }
  QChart *chart1 = new QChart();
  chart1->legend()->hide();
  chart1->addSeries(ofMonth);
  chart1->createDefaultAxes();
  chart1->axes(Qt::Horizontal).first()->setTitleText("Month");
  chart1->axes(Qt::Vertical).first()->setTitleText(
      QString::fromUtf8("Temperature (C\u00B0)"));
  chart1->setTitle("c.ii: Monthly maximum/minimum average and extrema");

  QChartView *chartView1 = new QChartView(chart1);
  chartView1->setRenderHint(QPainter::Antialiasing);
  chartView1->chart()->setTheme(QChart::ChartThemeDark);
  for (int i = 0; i < 12; i++) {
    QBoxSet *box = new QBoxSet(QString::number(i + 1));
    box->setValue(QBoxSet::LowerExtreme, min20[i]);
    box->setValue(QBoxSet::UpperExtreme, max20[i]);
    box->setValue(QBoxSet::Median, amim20[i]);
    box->setValue(QBoxSet::LowerQuartile, amim20[i]);
    box->setValue(QBoxSet::UpperQuartile, amam20[i]);
    of20y->append(box);
  }
  QChart *chart3 = new QChart();
  chart3->legend()->hide();
  chart3->addSeries(of20y);
  chart3->createDefaultAxes();
  chart3->axes(Qt::Horizontal).first()->setTitleText("Month");
  chart3->axes(Qt::Vertical).first()->setTitleText(
      QString::fromUtf8("Temperature (C\u00B0)"));
  chart3->setTitle(QString::fromStdString("d.ii: Monthly maximum/minimum "
                                          "average and extrema for " +
                                          to_string(year1) + " - " +
                                          to_string(year2)));

  QChartView *chartView3 = new QChartView(chart3);
  chartView3->setRenderHint(QPainter::Antialiasing);
  chartView3->chart()->setTheme(QChart::ChartThemeBlueIcy);
  for (int i = 0; i < 12; i++) {
    QBoxSet *box1 = new QBoxSet(QString::number(i + 1));
    QBoxSet *box = new QBoxSet(QString::number(i + 1));
    box->setValue(QBoxSet::LowerExtreme, amam[i] - stdmam[i]);
    box->setValue(QBoxSet::UpperExtreme, amam[i] + stdmam[i]);
    box->setValue(QBoxSet::Median, amam[i]);
    box->setValue(QBoxSet::LowerQuartile, amam[i]);
    box->setValue(QBoxSet::UpperQuartile, amam[i]);
    box1->setValue(QBoxSet::LowerExtreme, amim[i] - stdmim[i]);
    box1->setValue(QBoxSet::UpperExtreme, amim[i] + stdmim[i]);
    box1->setValue(QBoxSet::LowerQuartile, amim[i]);
    box1->setValue(QBoxSet::UpperQuartile, amim[i]);
    box1->setValue(QBoxSet::Median, amim[i]);
    meanStdoMa->append(box);
    meanStdoMi->append(box1);
  }
  QChart *chart4 = new QChart();
  chart4->legend()->hide();
  chart4->addSeries(meanStdoMa);
  chart4->addSeries(meanStdoMi);
  chart4->createDefaultAxes();
  chart4->axes(Qt::Horizontal).first()->setTitleText("Month");
  chart4->axes(Qt::Vertical).first()->setTitleText(
      QString::fromUtf8("Temperature (C\u00B0)"));
  chart4->setTitle("d.iv (Mean and std of min/max temperatures per month)");

  QChartView *chartView4 = new QChartView(chart4);
  chartView4->setRenderHint(QPainter::Antialiasing);
  chartView4->chart()->setTheme(QChart::ChartThemeQt);
  // d.iii
  // it is a lot harder to do this with operator< method due to invalid data
  auto maxDelta = ranges::max_element(
      days.begin(), days.end() - 1,
      // This kind of lambda expression is c++20 std. It is used to pass
      // one time used functions to the functions accepting functions as
      // arguments
      [](const DailyMeteoEvent &a, const DailyMeteoEvent &b) -> bool {
        // if statements are to push the invalid calculations towards the
        // minimum so that they will be omitted
        if (a.GetMaxT() == -274 ||
            days.at(days.indexOf(a) + 1).GetMaxT() == -274)
          return true;
        if (b.GetMaxT() == -274 ||
            days.at(days.indexOf(b) + 1).GetMaxT() == -274)
          return false;
        return (days.at(days.indexOf(a) + 1).GetMaxT() - a.GetMaxT()) <
               (days.at(days.indexOf(b) + 1).GetMaxT() - b.GetMaxT());
      });
  auto minDelta = ranges::min_element(
      days.begin(), days.end() - 1,
      // This kind of lambda expression is c++20 std. It is used to pass
      // one time used functions to the functions accepting functions as
      // arguments
      [](const DailyMeteoEvent &a, const DailyMeteoEvent &b) -> bool {
        // if statements are to push the invalid calculations towards the
        // maximum so that they will be omitted
        if (a.GetMaxT() == -274 ||
            days.at(days.indexOf(a) + 1).GetMaxT() == -274)
          return false;
        if (b.GetMaxT() == -274 ||
            days.at(days.indexOf(b) + 1).GetMaxT() == -274)
          return true;
        return (days.at(days.indexOf(a) + 1).GetMaxT() - a.GetMaxT()) <
               (days.at(days.indexOf(b) + 1).GetMaxT() - b.GetMaxT());
      });
  cout << "Largest decrease of max temperature is on: "
       << minDelta->GetJulianDay() << ", which decreases "
       << minDelta->GetMaxT() - days.at(days.indexOf(*minDelta) + 1).GetMaxT()
       << QString::fromUtf8(" C\u00B0").toStdString() << endl;
  cout << "Largest increase of max temperature is on: "
       << maxDelta->GetJulianDay() << ", which increases "
       << days.at(days.indexOf(*maxDelta) + 1).GetMaxT() - maxDelta->GetMaxT()
       << QString::fromUtf8(" C\u00B0").toStdString() << endl;
  // d.v
  QVector<QVector<double>> maxDay(31), minDay(31);
  QBoxPlotSeries *ofDay = new QBoxPlotSeries();
  /*for (int i = 0; i < 31; i++) {
    // a vector for each day
    QVector<double> maday, miday;
    maxDay.push_back(maday);
    minDay.push_back(miday);
  }*/
  for (auto &day : days) {
    if (day.GetMaxT() != -274.)
      maxDay[day.GetDay() - 1].push_back(day.GetMaxT());
    if (day.GetMinT() != -274.)
      minDay[day.GetDay() - 1].push_back(day.GetMinT());
  }
  QVector<double> maxInDay, minInDay,
      amad, // average max of days
      amid; // average min of days
  for (auto &m : maxDay) {
    maxInDay.push_back(*ranges::max_element(m));
    // accumulate is a c++20 std function to sum all elements in a iterable
    amad.push_back(accumulate(m.begin(), m.end(), 0) / m.count());
  }
  for (auto &m : minDay) {
    minInDay.push_back(*ranges::min_element(m));
    amid.push_back(accumulate(m.begin(), m.end(), 0) / m.count());
  }
  for (int i = 0; i < 31; i++) {
    QBoxSet *box = new QBoxSet(QString::number(i + 1));
    box->setValue(QBoxSet::LowerExtreme, minInDay[i]);
    box->setValue(QBoxSet::UpperExtreme, maxInDay[i]);
    box->setValue(QBoxSet::Median, amid[i]);
    box->setValue(QBoxSet::LowerQuartile, amid[i]);
    box->setValue(QBoxSet::UpperQuartile, amad[i]);
    ofDay->append(box);
  }
  QChart *chart2 = new QChart();
  chart2->legend()->hide();
  chart2->addSeries(ofDay);
  chart2->createDefaultAxes();
  chart2->axes(Qt::Horizontal).first()->setTitleText("Day");
  chart2->axes(Qt::Vertical).first()->setTitleText(
      QString::fromUtf8("Temperature (C\u00B0)"));
  chart2->setTitle("d.v: Daily maximum/minimum average and extrema");

  QChartView *chartView2 = new QChartView(chart2);
  chartView2->setRenderHint(QPainter::Antialiasing);
  chartView2->chart()->setTheme(QChart::ChartThemeBrownSand);
  // d.i
  QVector<QVector<QVector<double>>> maDayoY(12), miDayoY(12);
  for (int i = 0; i < 12; i++) {
    for (int j = 0; j < 31; j++) {
      QVector<double> maday, miday;
      maDayoY[i].push_back(maday);
      miDayoY[i].push_back(miday);
    }
  }
  for (auto &d : days) {
    maDayoY[d.GetMonth() - 1][d.GetDay() - 1].push_back(d.GetMaxT());
    miDayoY[d.GetMonth() - 1][d.GetDay() - 1].push_back(d.GetMinT());
  }
  for (int i = 0; i < 12; i++) {
    for (int j = 0; j < 31; j++) {
      if (maDayoY[i][j].size() < 2)
        continue; // for invalid days such as 31 February
      cout << i + 1 << "/" << j + 1 << " mean min: "
           << accumulate(miDayoY[i][j].begin(), miDayoY[i][j].end(), 0) /
                  miDayoY[i][j].size()
           << " mean max: "
           << accumulate(maDayoY[i][j].begin(), maDayoY[i][j].end(), 0) /
                  maDayoY[i][j].size()
           << " min extremum: " << *ranges::min_element(miDayoY[i][j])
           << " max extremum: " << *ranges::max_element(miDayoY[i][j]) << endl;
    }
  }
  // QT setup and graph layout
  QMainWindow window;
  QWidget *Window = new QWidget;
  QVBoxLayout *vbox1 = new QVBoxLayout(Window);
  QHBoxLayout *hbox1 = new QHBoxLayout();
  QHBoxLayout *hbox2 = new QHBoxLayout();
  QHBoxLayout *hbox3 = new QHBoxLayout();
  window.setCentralWidget(Window);
  vbox1->addLayout(hbox1);
  vbox1->addLayout(hbox2);
  vbox1->addLayout(hbox3);
  hbox1->addWidget(chartView0);
  hbox1->addWidget(chartView1);
  hbox2->addWidget(chartView2);
  hbox3->addWidget(chartView3);
  hbox3->addWidget(chartView4);

  window.resize(1920, 1010);
  window.setWindowTitle("Exercise 4");
  window.show();
  return a.exec();
}
