#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;
int jul_day(int day, int month, int year);

int main() {
  ifstream toread;
  toread.open("data.txt");
  ofstream towrite;
  towrite.open("data.csv");
  towrite << "day(int),month(int),year(int),JulianDay(int),MaxTemp(double),"
          << "MinTemp(double)" << endl;
  string line, field, day, month, year, cyear = "1960";
  while (getline(toread, line)) {
    // skip empty and unnecessary lines
    if (isdigit(line[0])) {
      // String::contains is a c++23 std func which checks whether a str
      // exists in in the string
      // skip dates without data
      if (line.contains("\t")) {
        stringstream streamData(line);
        getline(streamData, field, '\t');
        stringstream date(field);
        getline(date, day, '-');
        getline(date, month, '-');
        getline(date, year);
        // print 2 lines when year changes
        if (cyear != year) {
          towrite << endl << endl;
          cyear = year;
        }
        // stoi string to int func is a c++11 func to convert string to int
        towrite << day << ";" << month << ";" << year << ";"
                << jul_day(stoi(day), stoi(month), stoi(year)) << ";";
        getline(streamData, field, '\t');
        towrite << field << ";";
        if (line.substr(11).contains("\t")) {
            getline(streamData, field);
            towrite << field;
        }
        towrite << endl;
      }
    }
  }
  towrite.close();
  toread.close();
  return EXIT_SUCCESS;
}
int jul_day(int D, int M, int Y) {
  return div((1461 * (Y + 4800 + div((M - 14), 12).quot)), 4).quot +
         div((367 * (M - 2 - 12 * div((M - 14), 12).quot)), 12).quot -
         div((3 * ((Y + 4900 + (M - 14) / 12) / 100)), 4).quot + D - 32075;
}
