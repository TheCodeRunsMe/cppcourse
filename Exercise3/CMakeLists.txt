cmake_minimum_required(VERSION 3.5)

project(Exercise3 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(Exercise3 main.cpp)
