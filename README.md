# cpp Course Exercises

## Özgür Özer

Exercise 4 is coded with **QT 6** framework with qmake.

Environment info:
`Linux gnulinux 5.15.11-zen1-1-zen #1 ZEN SMP PREEMPT Wed, 22 Dec 2021 09:23:53 +0000 x86_64 GNU/Linux`

`Arch Gnu/Linux 64 bit gcc version 11.1.0 (GCC)`

## Exersice 1 Terminal io:

```
Enter tolerance:-1e-4
Enter lower bound:-3
Enter upper bound:3
Tolerance (entered:-0.000100) must be bigger than 0
Enter tolerance:1e-4
Function func should have different signs at the bounds a (entered:-3.000000) and b (entered:3.000000)
Enter lower bound:-4.2
Enter upper bound:-3.5
Function func should have different signs at the bounds a (entered:-4.200000) and b (entered:-3.500000)
Enter lower bound:-4.2
Enter upper bound:-3.2
Function func should have different signs at the bounds a (entered:-4.200000) and b (entered:-3.200000)
Enter lower bound:-4.2
Enter upper bound:-2.8
2.03675e-05 is the exact value of func at the root
15 is the number of iterations
-3.06998 is the root
Do you want to try again?(y/n)y
Enter tolerance:1e-4
Enter lower bound:-2.8
Enter upper bound:-4.2
The Lower bound a (entered:-2.800000) should be less than b (entered:-4.200000)
Enter lower bound:-4.2
Enter upper bound:2.8
4.51968e-05 is the exact value of func at the root
17 is the number of iterations
-1.77244 is the root
Do you want to try again?(y/n)n
```

## Exersice 2 Terminal io:

`Enter N: 1000`

## Exersice 4 Terminal io:

```
enter an arbitrary year to plot mothly means and extrema of next 20 years: 1976
Mean of julian day 2459215: 6.95
Absolute minimum temperature of all time is: -4.8 C°
Absolute maximum temperature of all time is: 46.6 C°
Largest decrease of max temperature is on: 2437827, which decreases 15 C°
Largest increase of max temperature is on: 2452193, which increases 12.3 C°
1/1 mean min: 5 mean max: 15 min extremum: -1.8 max extremum: 15.5
1/2 mean min: 5 mean max: 15 min extremum: -2.6 max extremum: 15
1/3 mean min: 5 mean max: 15 min extremum: -1.8 max extremum: 14.7
1/4 mean min: 5 mean max: 11 min extremum: -2.4 max extremum: 15.9
1/5 mean min: 5 mean max: 15 min extremum: -1 max extremum: 15.1
1/6 mean min: 5 mean max: 15 min extremum: -2 max extremum: 15
1/7 mean min: 4 mean max: 15 min extremum: -4.2 max extremum: 12.4
1/8 mean min: 4 mean max: 15 min extremum: -4.4 max extremum: 14
1/9 mean min: 5 mean max: 15 min extremum: -1.2 max extremum: 14.2
1/10 mean min: 5 mean max: 15 min extremum: -1.4 max extremum: 13
1/11 mean min: 5 mean max: 15 min extremum: -1.4 max extremum: 12.8
1/12 mean min: 4 mean max: 15 min extremum: -3.6 max extremum: 12.8
1/13 mean min: 4 mean max: 15 min extremum: -2.2 max extremum: 12.2
1/14 mean min: 4 mean max: 15 min extremum: -3.2 max extremum: 13.6
1/15 mean min: 4 mean max: 14 min extremum: -4.4 max extremum: 12.6
1/16 mean min: 4 mean max: 14 min extremum: -3.4 max extremum: 12
1/17 mean min: 5 mean max: 15 min extremum: -2 max extremum: 12.6
1/18 mean min: 5 mean max: 15 min extremum: -1.6 max extremum: 13.7
1/19 mean min: 5 mean max: 15 min extremum: -0.2 max extremum: 13
1/20 mean min: 5 mean max: 15 min extremum: -0.4 max extremum: 16
1/21 mean min: 1 mean max: 15 min extremum: -274 max extremum: 13.4
1/22 mean min: 5 mean max: 15 min extremum: -1.3 max extremum: 15
1/23 mean min: 5 mean max: 15 min extremum: -1.5 max extremum: 14
1/24 mean min: 5 mean max: 15 min extremum: -4 max extremum: 13.5
1/25 mean min: 5 mean max: 16 min extremum: -1 max extremum: 13.4
1/26 mean min: 5 mean max: 16 min extremum: -2.4 max extremum: 13.4
1/27 mean min: 5 mean max: 16 min extremum: -3.6 max extremum: 14
1/28 mean min: 5 mean max: 15 min extremum: -3.5 max extremum: 12.2
1/29 mean min: 5 mean max: 16 min extremum: -2.6 max extremum: 11.5
1/30 mean min: 1 mean max: 16 min extremum: -274 max extremum: 12.9
1/31 mean min: 5 mean max: 16 min extremum: -0.4 max extremum: 12.9
2/1 mean min: 5 mean max: 16 min extremum: 0.4 max extremum: 12
2/2 mean min: 5 mean max: 16 min extremum: -3.2 max extremum: 14
2/3 mean min: 5 mean max: 16 min extremum: -2.6 max extremum: 13.4
2/4 mean min: 5 mean max: 16 min extremum: -1.7 max extremum: 13.3
2/5 mean min: 5 mean max: 17 min extremum: -3 max extremum: 14.8
2/6 mean min: 5 mean max: 16 min extremum: -0.4 max extremum: 14
2/7 mean min: 6 mean max: 17 min extremum: 1.2 max extremum: 13.8
2/8 mean min: 5 mean max: 16 min extremum: 0.2 max extremum: 12.8
2/9 mean min: 6 mean max: 16 min extremum: -0.6 max extremum: 13.6
2/10 mean min: 6 mean max: 16 min extremum: -2 max extremum: 14.4
2/11 mean min: 6 mean max: 17 min extremum: -2.6 max extremum: 15
2/12 mean min: 6 mean max: 17 min extremum: -2.9 max extremum: 15.6
2/13 mean min: 6 mean max: 17 min extremum: -2.4 max extremum: 15.4
2/14 mean min: 6 mean max: 17 min extremum: -1.4 max extremum: 14.1
2/15 mean min: 6 mean max: 17 min extremum: -0.4 max extremum: 12.4
2/16 mean min: 6 mean max: 17 min extremum: -1.4 max extremum: 13.4
2/17 mean min: 5 mean max: 16 min extremum: -1.5 max extremum: 14.8
2/18 mean min: 6 mean max: 17 min extremum: -1 max extremum: 14.5
2/19 mean min: 6 mean max: 13 min extremum: 0 max extremum: 14.6
2/20 mean min: 6 mean max: 17 min extremum: -1 max extremum: 13.8
2/21 mean min: 6 mean max: 17 min extremum: -0.2 max extremum: 12.4
2/22 mean min: 6 mean max: 18 min extremum: -1.6 max extremum: 13.3
2/23 mean min: 6 mean max: 13 min extremum: 0.9 max extremum: 15
2/24 mean min: 6 mean max: 18 min extremum: -1 max extremum: 13.8
2/25 mean min: 2 mean max: 17 min extremum: -274 max extremum: 14.5
2/26 mean min: 7 mean max: 18 min extremum: 1.4 max extremum: 14.4
2/27 mean min: 7 mean max: 18 min extremum: 0.8 max extremum: 13.4
2/28 mean min: 7 mean max: 18 min extremum: 0.2 max extremum: 13.8
2/29 mean min: 7 mean max: 18 min extremum: 0.6 max extremum: 12.8
3/1 mean min: 3 mean max: 18 min extremum: -274 max extremum: 12.3
3/2 mean min: 6 mean max: 18 min extremum: -2 max extremum: 14
3/3 mean min: 7 mean max: 19 min extremum: 1 max extremum: 12.8
3/4 mean min: 7 mean max: 19 min extremum: 0.4 max extremum: 13.9
3/5 mean min: 7 mean max: 19 min extremum: 0 max extremum: 14
3/6 mean min: 7 mean max: 19 min extremum: 1.6 max extremum: 14
3/7 mean min: 7 mean max: 19 min extremum: 2.8 max extremum: 15.7
3/8 mean min: 7 mean max: 19 min extremum: 1 max extremum: 14
3/9 mean min: 7 mean max: 20 min extremum: 0.2 max extremum: 14.5
3/10 mean min: 7 mean max: 20 min extremum: 3 max extremum: 14
3/11 mean min: 8 mean max: 20 min extremum: 0.6 max extremum: 13.4
3/12 mean min: 8 mean max: 20 min extremum: 3.4 max extremum: 13.5
3/13 mean min: 8 mean max: 19 min extremum: 0.6 max extremum: 14
3/14 mean min: 3 mean max: 20 min extremum: -274 max extremum: 14.6
3/15 mean min: 3 mean max: 20 min extremum: -274 max extremum: 14.8
3/16 mean min: 7 mean max: 20 min extremum: 0.2 max extremum: 14.5
3/17 mean min: 7 mean max: 21 min extremum: 1.4 max extremum: 14
3/18 mean min: 8 mean max: 21 min extremum: 0.6 max extremum: 14
3/19 mean min: 8 mean max: 21 min extremum: 2.4 max extremum: 15.2
3/20 mean min: 8 mean max: 21 min extremum: 3.2 max extremum: 15.2
3/21 mean min: 8 mean max: 20 min extremum: 3.8 max extremum: 15.8
3/22 mean min: 8 mean max: 21 min extremum: 0.3 max extremum: 15.3
3/23 mean min: 8 mean max: 21 min extremum: 2 max extremum: 15
3/24 mean min: 9 mean max: 21 min extremum: 3.3 max extremum: 14.4
3/25 mean min: 9 mean max: 20 min extremum: 3 max extremum: 14.3
3/26 mean min: 8 mean max: 21 min extremum: 3 max extremum: 14.3
3/27 mean min: 9 mean max: 21 min extremum: 3.8 max extremum: 14.8
3/28 mean min: 9 mean max: 21 min extremum: 4.4 max extremum: 15.4
3/29 mean min: 8 mean max: 21 min extremum: 2.6 max extremum: 14.8
3/30 mean min: 8 mean max: 21 min extremum: 1.2 max extremum: 15.5
3/31 mean min: 8 mean max: 21 min extremum: 1.6 max extremum: 14.6
4/1 mean min: 9 mean max: 21 min extremum: 4.8 max extremum: 15.3
4/2 mean min: 9 mean max: 20 min extremum: 2.8 max extremum: 14.6
4/3 mean min: 9 mean max: 20 min extremum: 4.4 max extremum: 16.5
4/4 mean min: 9 mean max: 21 min extremum: 4.4 max extremum: 14.5
4/5 mean min: 9 mean max: 21 min extremum: 5 max extremum: 15
4/6 mean min: 9 mean max: 21 min extremum: 3 max extremum: 17
4/7 mean min: 9 mean max: 22 min extremum: 3.4 max extremum: 14.6
4/8 mean min: 10 mean max: 21 min extremum: 2.4 max extremum: 16.6
4/9 mean min: 9 mean max: 21 min extremum: 3.8 max extremum: 15.6
4/10 mean min: 9 mean max: 21 min extremum: 2.8 max extremum: 14.6
4/11 mean min: 9 mean max: 22 min extremum: 3.4 max extremum: 16.3
4/12 mean min: 10 mean max: 22 min extremum: 2.8 max extremum: 15.6
4/13 mean min: 10 mean max: 23 min extremum: 2.4 max extremum: 15.2
4/14 mean min: 10 mean max: 23 min extremum: 5.8 max extremum: 16.4
4/15 mean min: 10 mean max: 22 min extremum: 5.2 max extremum: 15.6
4/16 mean min: 10 mean max: 23 min extremum: 5.8 max extremum: 15.8
4/17 mean min: 10 mean max: 23 min extremum: 4 max extremum: 15.4
4/18 mean min: 10 mean max: 23 min extremum: 3.2 max extremum: 16.4
4/19 mean min: 10 mean max: 23 min extremum: 4.8 max extremum: 16
4/20 mean min: 10 mean max: 23 min extremum: 6 max extremum: 16
4/21 mean min: 10 mean max: 23 min extremum: 5.8 max extremum: 15.7
4/22 mean min: 10 mean max: 22 min extremum: 4 max extremum: 15.4
4/23 mean min: 10 mean max: 23 min extremum: 2.8 max extremum: 16.4
4/24 mean min: 10 mean max: 23 min extremum: 6 max extremum: 16.8
4/25 mean min: 11 mean max: 23 min extremum: 6.6 max extremum: 19.2
4/26 mean min: 11 mean max: 24 min extremum: 5.4 max extremum: 16.3
4/27 mean min: 11 mean max: 23 min extremum: 5.6 max extremum: 16.6
4/28 mean min: 11 mean max: 22 min extremum: 4 max extremum: 18.7
4/29 mean min: 10 mean max: 23 min extremum: 5.2 max extremum: 17.6
4/30 mean min: 11 mean max: 24 min extremum: 5.2 max extremum: 18.6
5/1 mean min: 11 mean max: 24 min extremum: 6.8 max extremum: 18.5
5/2 mean min: 11 mean max: 24 min extremum: 6.6 max extremum: 16.7
5/3 mean min: 11 mean max: 25 min extremum: 7.4 max extremum: 18.6
5/4 mean min: 12 mean max: 25 min extremum: 6.4 max extremum: 19
5/5 mean min: 12 mean max: 24 min extremum: 7.2 max extremum: 18
5/6 mean min: 12 mean max: 25 min extremum: 6 max extremum: 17.6
5/7 mean min: 12 mean max: 25 min extremum: 5.6 max extremum: 18.8
5/8 mean min: 12 mean max: 25 min extremum: 5.4 max extremum: 18
5/9 mean min: 12 mean max: 26 min extremum: 5 max extremum: 18.6
5/10 mean min: 13 mean max: 26 min extremum: 7 max extremum: 18
5/11 mean min: 13 mean max: 26 min extremum: 6.6 max extremum: 20.1
5/12 mean min: 13 mean max: 26 min extremum: 7.8 max extremum: 19.5
5/13 mean min: 8 mean max: 26 min extremum: -274 max extremum: 19.6
5/14 mean min: 12 mean max: 27 min extremum: 7.6 max extremum: 20.3
5/15 mean min: 13 mean max: 27 min extremum: 7.8 max extremum: 19.9
5/16 mean min: 13 mean max: 27 min extremum: 6.8 max extremum: 20.1
5/17 mean min: 13 mean max: 27 min extremum: 7.8 max extremum: 20.5
5/18 mean min: 13 mean max: 27 min extremum: 6.6 max extremum: 21.2
5/19 mean min: 13 mean max: 27 min extremum: 8 max extremum: 18.9
5/20 mean min: 13 mean max: 27 min extremum: 8.3 max extremum: 19
5/21 mean min: 13 mean max: 27 min extremum: 8 max extremum: 19.5
5/22 mean min: 13 mean max: 27 min extremum: 7.2 max extremum: 19
5/23 mean min: 13 mean max: 28 min extremum: 7.4 max extremum: 21
5/24 mean min: 14 mean max: 27 min extremum: 9.2 max extremum: 19.9
5/25 mean min: 14 mean max: 28 min extremum: 8 max extremum: 20.7
5/26 mean min: 14 mean max: 28 min extremum: 8 max extremum: 21.7
5/27 mean min: 14 mean max: 28 min extremum: 8.5 max extremum: 20.7
5/28 mean min: 14 mean max: 28 min extremum: 8.4 max extremum: 22.9
5/29 mean min: 14 mean max: 28 min extremum: 9.2 max extremum: 21.2
5/30 mean min: 15 mean max: 28 min extremum: 9.4 max extremum: 22
5/31 mean min: 15 mean max: 29 min extremum: 10.6 max extremum: 22.3
6/1 mean min: 15 mean max: 29 min extremum: 10.2 max extremum: 22.5
6/2 mean min: 15 mean max: 29 min extremum: 10 max extremum: 22.2
6/3 mean min: 15 mean max: 29 min extremum: 8.4 max extremum: 21.5
6/4 mean min: 15 mean max: 29 min extremum: 11.4 max extremum: 21
6/5 mean min: 15 mean max: 29 min extremum: 12.3 max extremum: 20
6/6 mean min: 15 mean max: 29 min extremum: 8.8 max extremum: 21.2
6/7 mean min: 15 mean max: 29 min extremum: 11.2 max extremum: 22.3
6/8 mean min: 15 mean max: 29 min extremum: 10.4 max extremum: 21.1
6/9 mean min: 16 mean max: 29 min extremum: 12 max extremum: 21.6
6/10 mean min: 15 mean max: 30 min extremum: 9.8 max extremum: 21.6
6/11 mean min: 16 mean max: 31 min extremum: 10.6 max extremum: 22
6/12 mean min: 16 mean max: 31 min extremum: 10 max extremum: 24.6
6/13 mean min: 16 mean max: 31 min extremum: 10 max extremum: 23.3
6/14 mean min: 16 mean max: 31 min extremum: 10 max extremum: 25.3
6/15 mean min: 16 mean max: 31 min extremum: 10 max extremum: 23.2
6/16 mean min: 16 mean max: 31 min extremum: 11 max extremum: 23.3
6/17 mean min: 17 mean max: 32 min extremum: 10.2 max extremum: 23.8
6/18 mean min: 17 mean max: 31 min extremum: 10 max extremum: 26.6
6/19 mean min: 17 mean max: 32 min extremum: 11 max extremum: 23.6
6/20 mean min: 17 mean max: 32 min extremum: 12.4 max extremum: 24.6
6/21 mean min: 17 mean max: 32 min extremum: 11.8 max extremum: 24.6
6/22 mean min: 18 mean max: 32 min extremum: 12.6 max extremum: 24.7
6/23 mean min: 17 mean max: 32 min extremum: 11.6 max extremum: 23
6/24 mean min: 17 mean max: 32 min extremum: 12.6 max extremum: 23.9
6/25 mean min: 17 mean max: 32 min extremum: 14 max extremum: 24.3
6/26 mean min: 17 mean max: 32 min extremum: 13.6 max extremum: 24.3
6/27 mean min: 17 mean max: 32 min extremum: 12.8 max extremum: 24.6
6/28 mean min: 18 mean max: 33 min extremum: 15 max extremum: 24.4
6/29 mean min: 18 mean max: 33 min extremum: 14.2 max extremum: 25
6/30 mean min: 18 mean max: 33 min extremum: 12.6 max extremum: 25.9
7/1 mean min: 18 mean max: 33 min extremum: 15 max extremum: 24
7/2 mean min: 18 mean max: 33 min extremum: 12.8 max extremum: 24.8
7/3 mean min: 18 mean max: 33 min extremum: 14.2 max extremum: 24.4
7/4 mean min: 18 mean max: 33 min extremum: 14.7 max extremum: 23.2
7/5 mean min: 18 mean max: 33 min extremum: 14.6 max extremum: 24.4
7/6 mean min: 18 mean max: 34 min extremum: 15 max extremum: 24.4
7/7 mean min: 18 mean max: 34 min extremum: 14.2 max extremum: 26
7/8 mean min: 18 mean max: 34 min extremum: 14 max extremum: 23.8
7/9 mean min: 18 mean max: 34 min extremum: 13.5 max extremum: 23.5
7/10 mean min: 19 mean max: 35 min extremum: 14.2 max extremum: 24
7/11 mean min: 19 mean max: 35 min extremum: 14 max extremum: 27.8
7/12 mean min: 19 mean max: 35 min extremum: 14.4 max extremum: 26.4
7/13 mean min: 19 mean max: 35 min extremum: 14.2 max extremum: 24.6
7/14 mean min: 19 mean max: 35 min extremum: 14.2 max extremum: 24.6
7/15 mean min: 19 mean max: 35 min extremum: 14.3 max extremum: 24
7/16 mean min: 19 mean max: 35 min extremum: 14.6 max extremum: 26
7/17 mean min: 19 mean max: 36 min extremum: 14.8 max extremum: 26.8
7/18 mean min: 19 mean max: 36 min extremum: 14.8 max extremum: 26.4
7/19 mean min: 19 mean max: 36 min extremum: 15 max extremum: 25.6
7/20 mean min: 19 mean max: 36 min extremum: 14.4 max extremum: 25.6
7/21 mean min: 19 mean max: 36 min extremum: 15.8 max extremum: 27
7/22 mean min: 19 mean max: 35 min extremum: 16.2 max extremum: 25.4
7/23 mean min: 19 mean max: 35 min extremum: 15.5 max extremum: 26.4
7/24 mean min: 19 mean max: 36 min extremum: 15.6 max extremum: 27
7/25 mean min: 19 mean max: 35 min extremum: 16.6 max extremum: 29.3
7/26 mean min: 19 mean max: 35 min extremum: 16 max extremum: 26
7/27 mean min: 19 mean max: 35 min extremum: 14.8 max extremum: 24.7
7/28 mean min: 19 mean max: 36 min extremum: 15 max extremum: 25
7/29 mean min: 19 mean max: 35 min extremum: 14.6 max extremum: 25.8
7/30 mean min: 19 mean max: 35 min extremum: 14.6 max extremum: 27
7/31 mean min: 19 mean max: 35 min extremum: 14.3 max extremum: 25.2
8/1 mean min: 19 mean max: 35 min extremum: 14.6 max extremum: 25
8/2 mean min: 19 mean max: 35 min extremum: 14.8 max extremum: 26.8
8/3 mean min: 19 mean max: 35 min extremum: 13.8 max extremum: 24.8
8/4 mean min: 19 mean max: 36 min extremum: 13.8 max extremum: 24.7
8/5 mean min: 19 mean max: 36 min extremum: 14.2 max extremum: 24.5
8/6 mean min: 19 mean max: 36 min extremum: 13.8 max extremum: 25.4
8/7 mean min: 19 mean max: 35 min extremum: 15 max extremum: 27.8
8/8 mean min: 19 mean max: 35 min extremum: 14.6 max extremum: 26.6
8/9 mean min: 19 mean max: 35 min extremum: 15 max extremum: 26.8
8/10 mean min: 19 mean max: 35 min extremum: 14 max extremum: 27.1
8/11 mean min: 19 mean max: 35 min extremum: 14.4 max extremum: 26.5
8/12 mean min: 19 mean max: 35 min extremum: 13 max extremum: 26.5
8/13 mean min: 19 mean max: 35 min extremum: 15 max extremum: 25.4
8/14 mean min: 19 mean max: 35 min extremum: 14 max extremum: 25.4
8/15 mean min: 19 mean max: 35 min extremum: 15 max extremum: 25
8/16 mean min: 19 mean max: 35 min extremum: 14 max extremum: 24.8
8/17 mean min: 18 mean max: 35 min extremum: 14.4 max extremum: 24.5
8/18 mean min: 18 mean max: 35 min extremum: 13.6 max extremum: 24.6
8/19 mean min: 19 mean max: 35 min extremum: 14.4 max extremum: 24.5
8/20 mean min: 19 mean max: 35 min extremum: 14.8 max extremum: 28.3
8/21 mean min: 19 mean max: 35 min extremum: 13.6 max extremum: 24.7
8/22 mean min: 19 mean max: 35 min extremum: 13.4 max extremum: 25
8/23 mean min: 19 mean max: 35 min extremum: 14 max extremum: 24.4
8/24 mean min: 19 mean max: 35 min extremum: 13.2 max extremum: 24.6
8/25 mean min: 19 mean max: 34 min extremum: 13.4 max extremum: 25.8
8/26 mean min: 18 mean max: 34 min extremum: 14 max extremum: 23.4
8/27 mean min: 18 mean max: 34 min extremum: 13.8 max extremum: 23.4
8/28 mean min: 18 mean max: 33 min extremum: 14 max extremum: 24.5
8/29 mean min: 18 mean max: 33 min extremum: 13.6 max extremum: 23.2
8/30 mean min: 18 mean max: 34 min extremum: 12.2 max extremum: 24.3
8/31 mean min: 18 mean max: 33 min extremum: 13.2 max extremum: 24.1
9/1 mean min: 18 mean max: 33 min extremum: 12.4 max extremum: 23.2
9/2 mean min: 18 mean max: 33 min extremum: 14 max extremum: 23.4
9/3 mean min: 18 mean max: 33 min extremum: 12.6 max extremum: 23.2
9/4 mean min: 18 mean max: 33 min extremum: 11.6 max extremum: 24.3
9/5 mean min: 18 mean max: 32 min extremum: 12.3 max extremum: 23.3
9/6 mean min: 18 mean max: 33 min extremum: 12.6 max extremum: 23.2
9/7 mean min: 18 mean max: 33 min extremum: 13 max extremum: 23.5
9/8 mean min: 18 mean max: 33 min extremum: 13.8 max extremum: 23
9/9 mean min: 18 mean max: 32 min extremum: 13.6 max extremum: 23.8
9/10 mean min: 18 mean max: 32 min extremum: 12.6 max extremum: 23.5
9/11 mean min: 17 mean max: 32 min extremum: 12 max extremum: 22.4
9/12 mean min: 17 mean max: 32 min extremum: 10.8 max extremum: 22
9/13 mean min: 17 mean max: 32 min extremum: 10.2 max extremum: 23.3
9/14 mean min: 17 mean max: 31 min extremum: 11.2 max extremum: 23.4
9/15 mean min: 17 mean max: 31 min extremum: 11.8 max extremum: 23.3
9/16 mean min: 17 mean max: 30 min extremum: 8.6 max extremum: 23.4
9/17 mean min: 17 mean max: 30 min extremum: 10.4 max extremum: 21.6
9/18 mean min: 17 mean max: 30 min extremum: 9 max extremum: 21.6
9/19 mean min: 16 mean max: 30 min extremum: 9 max extremum: 21.8
9/20 mean min: 16 mean max: 30 min extremum: 11 max extremum: 22
9/21 mean min: 17 mean max: 30 min extremum: 11.6 max extremum: 22.2
9/22 mean min: 16 mean max: 30 min extremum: 12 max extremum: 22.4
9/23 mean min: 16 mean max: 30 min extremum: 10.6 max extremum: 21
9/24 mean min: 16 mean max: 30 min extremum: 10.4 max extremum: 20.6
9/25 mean min: 16 mean max: 29 min extremum: 10.4 max extremum: 21
9/26 mean min: 16 mean max: 29 min extremum: 11 max extremum: 22
9/27 mean min: 16 mean max: 29 min extremum: 11.2 max extremum: 22.2
9/28 mean min: 16 mean max: 29 min extremum: 10 max extremum: 22.8
9/29 mean min: 16 mean max: 28 min extremum: 11.2 max extremum: 21.6
9/30 mean min: 15 mean max: 28 min extremum: 9 max extremum: 20.9
10/1 mean min: 15 mean max: 28 min extremum: 10.6 max extremum: 22.4
10/2 mean min: 15 mean max: 28 min extremum: 7.8 max extremum: 21.6
10/3 mean min: 15 mean max: 28 min extremum: 6.8 max extremum: 20.9
10/4 mean min: 15 mean max: 28 min extremum: 8.2 max extremum: 19.6
10/5 mean min: 14 mean max: 27 min extremum: 8.8 max extremum: 20.2
10/6 mean min: 14 mean max: 27 min extremum: 9.6 max extremum: 19.6
10/7 mean min: 14 mean max: 27 min extremum: 9.6 max extremum: 19.7
10/8 mean min: 14 mean max: 27 min extremum: 9 max extremum: 20.6
10/9 mean min: 14 mean max: 27 min extremum: 7.6 max extremum: 20
10/10 mean min: 14 mean max: 26 min extremum: 8.2 max extremum: 21
10/11 mean min: 9 mean max: 26 min extremum: -274 max extremum: 20.1
10/12 mean min: 14 mean max: 25 min extremum: 8.2 max extremum: 19.8
10/13 mean min: 13 mean max: 25 min extremum: 5.6 max extremum: 19.2
10/14 mean min: 13 mean max: 25 min extremum: 6.6 max extremum: 19.5
10/15 mean min: 13 mean max: 24 min extremum: 6.6 max extremum: 19.4
10/16 mean min: 13 mean max: 24 min extremum: 6.6 max extremum: 18.2
10/17 mean min: 13 mean max: 24 min extremum: 4.6 max extremum: 19.2
10/18 mean min: 13 mean max: 24 min extremum: 7 max extremum: 19.7
10/19 mean min: 13 mean max: 24 min extremum: 7.2 max extremum: 19.2
10/20 mean min: 13 mean max: 24 min extremum: 2.8 max extremum: 21.1
10/21 mean min: 8 mean max: 24 min extremum: -274 max extremum: 19.3
10/22 mean min: 13 mean max: 24 min extremum: 7 max extremum: 18
10/23 mean min: 12 mean max: 24 min extremum: 6.8 max extremum: 18.4
10/24 mean min: 13 mean max: 23 min extremum: 6 max extremum: 19.3
10/25 mean min: 12 mean max: 24 min extremum: 6 max extremum: 17.5
10/26 mean min: 12 mean max: 23 min extremum: 2 max extremum: 18.2
10/27 mean min: 12 mean max: 23 min extremum: 4 max extremum: 19.2
10/28 mean min: 11 mean max: 23 min extremum: 3.8 max extremum: 18.5
10/29 mean min: 11 mean max: 24 min extremum: 3.8 max extremum: 20
10/30 mean min: 11 mean max: 23 min extremum: 4.4 max extremum: 18.4
10/31 mean min: 11 mean max: 23 min extremum: 2.2 max extremum: 18.1
11/1 mean min: 11 mean max: 23 min extremum: 0 max extremum: 17.8
11/2 mean min: 11 mean max: 22 min extremum: 0 max extremum: 18.6
11/3 mean min: 11 mean max: 21 min extremum: 3.5 max extremum: 18.2
11/4 mean min: 11 mean max: 21 min extremum: 2.5 max extremum: 17.8
11/5 mean min: 11 mean max: 21 min extremum: 5.6 max extremum: 18.6
11/6 mean min: 10 mean max: 20 min extremum: 4 max extremum: 18.6
11/7 mean min: 10 mean max: 20 min extremum: 4.3 max extremum: 16.4
11/8 mean min: 10 mean max: 20 min extremum: 4.4 max extremum: 17
11/9 mean min: 10 mean max: 20 min extremum: 4.3 max extremum: 16.5
11/10 mean min: 9 mean max: 20 min extremum: 4.3 max extremum: 16.4
11/11 mean min: 10 mean max: 20 min extremum: 2.3 max extremum: 16
11/12 mean min: 9 mean max: 20 min extremum: 0.8 max extremum: 18
11/13 mean min: 9 mean max: 20 min extremum: -0.6 max extremum: 17.1
11/14 mean min: 9 mean max: 19 min extremum: 0 max extremum: 18
11/15 mean min: 9 mean max: 19 min extremum: 1.6 max extremum: 15.7
11/16 mean min: 8 mean max: 19 min extremum: 1.3 max extremum: 17.4
11/17 mean min: 8 mean max: 19 min extremum: 2.8 max extremum: 19
11/18 mean min: 8 mean max: 19 min extremum: 3.6 max extremum: 16
11/19 mean min: 8 mean max: 19 min extremum: 1 max extremum: 16.5
11/20 mean min: 8 mean max: 18 min extremum: 1 max extremum: 17.6
11/21 mean min: 8 mean max: 18 min extremum: 1.8 max extremum: 16.4
11/22 mean min: 8 mean max: 18 min extremum: 1 max extremum: 16
11/23 mean min: 7 mean max: 17 min extremum: 0.5 max extremum: 15
11/24 mean min: 7 mean max: 17 min extremum: 1 max extremum: 16.6
11/25 mean min: 8 mean max: 18 min extremum: 1 max extremum: 14.5
11/26 mean min: 8 mean max: 18 min extremum: 0.5 max extremum: 14.6
11/27 mean min: 7 mean max: 17 min extremum: -1.4 max extremum: 14.4
11/28 mean min: 6 mean max: 17 min extremum: -0.5 max extremum: 14
11/29 mean min: 6 mean max: 17 min extremum: -0.6 max extremum: 13.4
11/30 mean min: 6 mean max: 16 min extremum: 0.4 max extremum: 13
12/1 mean min: 6 mean max: 16 min extremum: -1.4 max extremum: 13.8
12/2 mean min: 6 mean max: 17 min extremum: -4.2 max extremum: 14.4
12/3 mean min: 6 mean max: 16 min extremum: -3.6 max extremum: 13.8
12/4 mean min: 6 mean max: 16 min extremum: -1 max extremum: 13.5
12/5 mean min: 6 mean max: 17 min extremum: -1.4 max extremum: 13.9
12/6 mean min: 6 mean max: 16 min extremum: -2.6 max extremum: 15.8
12/7 mean min: 7 mean max: 16 min extremum: -2.4 max extremum: 16.6
12/8 mean min: 6 mean max: 16 min extremum: 0 max extremum: 16.8
12/9 mean min: 6 mean max: 16 min extremum: -2 max extremum: 15.4
12/10 mean min: 6 mean max: 17 min extremum: 0 max extremum: 16
12/11 mean min: 7 mean max: 16 min extremum: -2 max extremum: 16.3
12/12 mean min: 6 mean max: 16 min extremum: -2.4 max extremum: 14.4
12/13 mean min: 7 mean max: 16 min extremum: -4.8 max extremum: 17
12/14 mean min: 6 mean max: 16 min extremum: -0.6 max extremum: 16.8
12/15 mean min: 6 mean max: 15 min extremum: -1.6 max extremum: 15.1
12/16 mean min: 6 mean max: 15 min extremum: -0.6 max extremum: 15.6
12/17 mean min: 6 mean max: 15 min extremum: -0.8 max extremum: 14.4
12/18 mean min: 6 mean max: 15 min extremum: -2 max extremum: 15.6
12/19 mean min: 6 mean max: 15 min extremum: -1.6 max extremum: 13.2
12/20 mean min: 6 mean max: 15 min extremum: -0.6 max extremum: 17.3
12/21 mean min: 6 mean max: 16 min extremum: -0.8 max extremum: 15
12/22 mean min: 6 mean max: 16 min extremum: -4.8 max extremum: 15.2
12/23 mean min: 5 mean max: 15 min extremum: -0.6 max extremum: 14.6
12/24 mean min: 6 mean max: 15 min extremum: -2.4 max extremum: 16.4
12/25 mean min: 5 mean max: 15 min extremum: -4.6 max extremum: 16.5
12/26 mean min: 5 mean max: 15 min extremum: -2.2 max extremum: 14.6
12/27 mean min: 5 mean max: 15 min extremum: -2.5 max extremum: 13.5
12/28 mean min: 5 mean max: 15 min extremum: -1 max extremum: 13.7
12/29 mean min: 6 mean max: 15 min extremum: -0.6 max extremum: 16.4
12/30 mean min: 6 mean max: 15 min extremum: -1.5 max extremum: 15.8
12/31 mean min: 5 mean max: 15 min extremum: -2.6 max extremum: 15
```

![Exercise 4 Plot](Exercise4/plotOfExercise4.png)




