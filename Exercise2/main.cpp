#include <algorithm>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;
long double func(const long double &x) { return sin(pow(x, 2)); }

class LagInt {
  vector<long double> x, y;
public:
  LagInt(const vector<long double> &xi, const vector<long double> &yi)
  : x(xi), y(yi) {
    if (xi.size() != yi.size()) throw length_error("Vector sizes are different");
  };
  long double eval(const long double &input) {
    long double res{0};
    for (int i = 0; i < x.size(); i++) {
      long double prod = 1;
      for (int j = 0; j < x.size(); j++)
        if (j != i) prod *= (input - x[j]) / (x[i] - x[j]);
      res += prod * y[i];
    }
    return res;
  }
};

int main() {
  int N; cout << "Enter N: "; cin >> N;
  if (N < 1) throw domain_error("N should be bigger than 0");
  vector<long double> y(5), x, genx, geny;
  // generated 5 x points to interpolate
  for (long double i = -M_PI / 2; i < 1.1 * M_PI / 2; i += M_PI / 4)
    x.push_back(i);
  // generated 5 y points
  // C++ std 20 transform algorithm here applies func for each x and saves to y
  ranges::transform(x.begin(), x.end(), y.begin(), func);

  LagInt interpolator{x, y};
  // generate N x,y points. x gives the same sequence regardless of N(precalculated)
  for (int i = 1; i <= N; i++) {
    genx.push_back(M_PI / i / 2);
    geny.push_back(interpolator.eval(M_PI / i / 2));
  }
  ofstream ofile;
  ofile.open("Generated Points.csv");
  for (int i = 0; i < genx.size(); i++)
    ofile << genx[i] << "," << geny[i] << endl;
  ofile.close();
  return EXIT_SUCCESS;
}

