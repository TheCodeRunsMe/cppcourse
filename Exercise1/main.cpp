#include <cmath>
#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;

double func(const double &x);

double GetRootBisec(double a, double b, double tol);

int main() {
  string query;
  do {
    double tolerance, a, b;
    cout << "Enter tolerance:"; cin >> tolerance;
    cout << "Enter lower bound:"; cin >> a;
    cout << "Enter upper bound:"; cin >> b;
    while (true) {
      try {
        cout << GetRootBisec(a, b, tolerance) << " is the root" << endl;
      } catch (invalid_argument &e1) {
        cout << e1.what() << endl;
        cout << "Enter lower bound:"; cin >> a;
        cout << "Enter upper bound:"; cin >> b;
        continue;
      } catch (domain_error &e2) {
        cout << e2.what() << endl;
        cout << "Enter tolerance:"; cin >> tolerance;
        continue;
      } catch (logic_error &e3) {
        cout << e3.what() << endl;
        cout << "Enter lower bound:"; cin >> a;
        cout << "Enter upper bound:"; cin >> b;
        continue;
      }
      break;
    }
    cout << "Do you want to try again?(y/n)"; cin >> query;
  } while (query != "n");
  return EXIT_SUCCESS;
}

double GetRootBisec(double a, double b, double tol) {
  // domain_error and invalid_argument are a std-cpp11 stdlib exceptions
  // In '"string"s' s prefix makes the "string" a sting literal in std-cpp17
  // so that it is possible to use string features
  if (a > b)
    throw invalid_argument("The Lower bound a (entered:"s + to_string(a) +
                           ") should be less than b (entered:"s + to_string(b) +
                           ")"s);
  if (tol < 0)
    throw domain_error("Tolerance (entered:"s + to_string(tol) +
                       ") must be bigger than 0"s);
  if (func(a) * func(b) > 0)
    throw logic_error(
        "Function func should have different signs at the bounds a (entered:"s +
        to_string(a) + ") and b (entered:"s + to_string(b) + ")"s);
  int numberOfIteration = 0;
  double m;
  do {
    m = (a + b) / 2;
    (func(a) * func(m) > 0) ? a = m : b = m;
    numberOfIteration++;
  } while (abs(func(m)) > tol);
  cout << func(m) << " is the exact value of func at the root" << endl
       << numberOfIteration << " is the number of iterations" << endl;
  return m;
}

double func(const double &x) { return sin(pow(x, 2)); }
